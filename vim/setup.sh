#!/bin/bash

setup() {
	echo "Linking .vimrc file"
}

link_dotfiles() {
	ln -s $HOME/.dotfiles/vim/.vimrc $HOME/.vimrc
}


setup
link_dotfiles


#!/bin/bash

setup() {
	[ ! -f "/bin/zsh" ] && sudo apt-get install zsh
	[ ! -d "$HOME/.oh-my-zsh" ] && curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh
}

link_dotfiles() {
	ln -s $HOME/.dotfiles/omz/.zshrc $HOME/.zshrc
}


setup
link_dotfiles


#!/bin/bash


add_setup_dir() {
	cd $1
	./setup.sh
	cd ..
}


for directory_with_slash in */
do
       directory="${directory_with_slash:0:$((${#directory_with_slash}-1))}"
       echo "Setting up $directory..."
       add_setup_dir $directory
done

echo "
Done!
"


#!/bin/bash

setup() {
	mkdir -p $HOME/.tmux/plugins
	[ ! -d "$HOME/.tmux/plugins/tpm" ] && git clone https://github.com/tmux-plugins/tpm "$HOME/.tmux/plugins/tpm"
	echo "Make sure to Prefix+I to install all the configured plugins in tmux"
}

link_dotfiles() {
	ln -s $HOME/.dotfiles/tmux/.tmux.conf $HOME/.tmux.conf
}


setup
link_dotfiles
